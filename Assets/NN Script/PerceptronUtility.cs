﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class inputPerceptron
{
    public float input;
    public float weights;
}

public class Perceptron:inputPerceptron
{
    public inputPerceptron[] inputList;
    public delegate float Threshold(float x);
    public Threshold threshold;
    public float state;
    public float error;

    public Perceptron(int inputSize)
    {
        inputList = new inputPerceptron[inputSize];
    }

    public void feedForward()
    {
        float sum = 0.0f;
        foreach(inputPerceptron i in inputList)
        {
            sum += i.input * i.weights;
        }

        state = threshold(sum);
    }

    public void adjustWeights(float currentError)
    {
        for(int i = 0;i<inputList.Length;i++)
        {
            float deltaWeight;
            deltaWeight = currentError * inputList[i].weights * state;
            inputList[i].weights = deltaWeight;
            error = currentError;
        }
    }

    public float getIncomingWeight()
    {
        foreach(inputPerceptron i in inputList)
        {
            if (i.GetType() == typeof(Perceptron))
                return i.weights;
        }

        return 0.0f;
    }
}

