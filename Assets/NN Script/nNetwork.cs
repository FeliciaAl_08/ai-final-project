﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class nNetwork : MonoBehaviour
{
    public Perceptron[] inputLayer;
    public Perceptron[] hiddenLayer;
    public Perceptron[] outputLayer;

    public void generateOutput(Perceptron[] inputs)
    {
        for (int i = 0; i < inputs.Length; i++)
            inputLayer[i].state = inputs[i].input;

        for (int i = 0; i < hiddenLayer.Length; i++)
            hiddenLayer[i].feedForward();

        for (int i = 0; i < outputLayer.Length; i++)
            outputLayer[i].feedForward();
    }

    public void backProp(Perceptron[] outputs)
    {
        for(int i = 0; i<outputLayer.Length;i++)
        {
            Perceptron p = outputLayer[i];

            float state = p.state;
            float error = state * (1.0f - state);
            error *= outputs[i].state - state;
            p.adjustWeights(error);
        }

        for(int i = 0; i<hiddenLayer.Length;i++)
        {
            Perceptron p = outputLayer[i];
            float state = p.state;
            float sum = 0.0f;

            for(int j = 0;j<outputs.Length;j++)
            {
                float incomingW = outputs[i].getIncomingWeight();
                sum += incomingW * outputs[i].error;
                float error = state * (1.0f - state) * sum;
                p.adjustWeights(error);
            }
        }
    }

    public void Learn (Perceptron[] inputs, Perceptron[] outputs)
    {
        generateOutput(inputs);
        backProp(outputs);
    }
}
