﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Screenshot : MonoBehaviour
{
    public bool grab;
    public static void ScreenshotTool()
    {
        string filePath = Application.dataPath + "/" + "screenshot" + System.DateTime.Now.Day.ToString() + ".png";

        Texture2D tex = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);

        tex.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);

        byte[] bytes = tex.EncodeToPNG();

        File.WriteAllBytes(filePath, bytes);
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
