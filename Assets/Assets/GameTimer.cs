﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameTimer : MonoBehaviour {
    public static float time = 10.0f;
    //public static bool count = false;
    public string DisplayTime;

    public Text Timer;

   // string TimerField = "10";

    string TimerDisplay;
	// Use this for initialization
	void Start ()
    {
        TimerDisplay = time.ToString();
	}
	
	// Update is called once per frame
	public void Update ()
    {
	    if(enabled)
        {
            time -= Time.deltaTime;
            //print(time);
            TimerDisplay = time.ToString();
            Timer.text = TimerDisplay;
            if(time <=0)
            {
                print("Game Over");
                Game_.GameOver();
            }
        }

        if (!enabled) return;
	}

   

    private void OnGUI()
    {
        GUI.Label(new Rect(10, 40, 100, 20), TimerDisplay);
    }

}
