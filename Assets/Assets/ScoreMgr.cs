﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreMgr : MonoBehaviour
{
    public static int score;
    public static int healthPoint;

    public Text ScoreText;
    public Text healthText;
	// Use this for initialization
	void Start ()
    {
	}

    private void Awake()
    {
        score = 0;
        healthPoint = 10;

    }

    // Update is called once per frame
    void Update ()
    {
        ScoreText.text = score.ToString();
        healthText.text = healthPoint.ToString();

        if(healthPoint == 0)
        {
            Game_.GameOver();
        }
	}
}
