﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour {
    public GameObject m_object;

    //int score;
    string scoreDisplay;
    public static int scoreValue = 1;
    //public Collider2D hit;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
	    	
	}

    void OnCollisionEnter2D(Collision2D other)
    {

        if (other.gameObject.tag == "Player")
        {
            destroyCoin();
            Fmod.updateChannelPos(transform.position.x, transform.position.y, transform.position.z);
            Fmod.updateChannelVel(0f, 0f, 0f);
            Fmod.playSounds(1);
        }
    }

    void destroyCoin()
    {
        Destroy(gameObject);
        print("Collision");

        ScoreMgr.score += scoreValue;
    }

    private void OnGUI()
    {

    }
}
