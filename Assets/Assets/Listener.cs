﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Listener : MonoBehaviour
{
    public GameObject m_player;
    Rigidbody2D m_RB;

    // Use this for initialization
    void Start ()
    {

        //C:\Users\100526516\Documents\Assignment3\Assets\Assets\Sound
        Fmod.initialize(3);
        Fmod.createSound(Application.dataPath+"/Assets/Sound/baap.wav", 0);
        Fmod.createSound(Application.dataPath+"/Assets/Sound/ding.wav", 1);
        Fmod.createSound(Application.dataPath + "/Assets/Sound/bloop.wav", 2);

        //Fmod.playSounds(1);
        m_RB = m_player.GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        Fmod.updateListenerPos(transform.position.x, transform.position.y, transform.position.z);
        Fmod.updateListenerVel(m_RB.velocity.x, m_RB.velocity.y,0);
        Fmod.updateListenerUp(transform.up.x, transform.up.y, transform.up.z);
        Fmod.updateListenerFor(transform.forward.x, transform.forward.y, transform.forward.z);
        Fmod.update();
	}
}
