﻿using System;
using System.Runtime.InteropServices;

public class DLLTest
{
    [DllImport("DLL")]
    public static extern float TestMultiply(float a, float b);

    [DllImport("DLL")]
    public static extern float TestAdd(float a, float b);

    [DllImport("DLL")]
    public static extern void Init([MarshalAs(UnmanagedType.LPStr)]string filename);

    [DllImport("DLL")]
    public static extern int getNum();

    [DllImport("DLL")]
    public static extern IntPtr getLine(int ID);
    
    [DllImport("DLL")]
    public static extern IntPtr getCharacterName(int ID);
}