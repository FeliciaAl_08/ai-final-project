﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawn : MonoBehaviour
{
    public static void SpawnCoin(Transform coin)
    {
        Vector3 mousePos = Camera.current.ScreenToWorldPoint(Input.mousePosition);

        Vector3 objPos = new Vector3(mousePos.x, mousePos.y - 2.0f, 0.0f);
        //objPos.z = 0.0f;
        Instantiate(coin, objPos, Quaternion.Euler(new Vector3(0, 0, 0)));

        //coin.GetComponent<CoinAnim>().enabled = false;

    }
}