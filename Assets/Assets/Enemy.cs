﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{
    public LayerMask enemyMask;
    public float speed = 1;
    Rigidbody2D myBody;
    Transform myTrans;
    float myWidth, myHeight;

    void Start()
    {
        myTrans = this.transform;
        myBody = this.GetComponent<Rigidbody2D>();
        SpriteRenderer mySprite = this.GetComponent<SpriteRenderer>();
        myWidth = mySprite.bounds.extents.x;
        myHeight = mySprite.bounds.extents.y;
    }

    private Vector2 TempPos2;
    private Vector2 TempRight2;
    private Vector2 TempUp2;

    void FixedUpdate()
    {
        TempPos2 = myTrans.position;
        TempUp2 = myTrans.up;
        //TempPos2 = new Vector2(myTrans.position.x, myTrans.position.y - 0.5f);
        TempRight2 = myTrans.right;

        //Use this position to cast the isGrounded/isBlocked lines from
        Vector2 lineCastPos = TempPos2 - TempRight2 * myWidth + Vector2.up * myHeight;

        //Check to see if there's ground in front of us before moving forward
        Debug.DrawLine(lineCastPos, lineCastPos + Vector2.down);
        bool isGrounded = Physics2D.Linecast(lineCastPos, lineCastPos + Vector2.down, enemyMask);

        //Check to see if there's a wall in front of us before moving forward
        Debug.DrawLine(lineCastPos, lineCastPos - TempRight2 * .05f);
        bool isBlocked = Physics2D.Linecast(lineCastPos, lineCastPos - TempRight2 * .05f, enemyMask);
        Debug.DrawLine(lineCastPos + TempRight2 * myHeight, lineCastPos - TempRight2 * .05f + TempUp2 * myHeight);
        RaycastHit2D isBlocked2 = Physics2D.Linecast(lineCastPos + TempRight2 * myHeight, lineCastPos - TempRight2 * .05f + TempUp2 * myHeight, enemyMask);

        //If theres no ground, turn around. Or if I hit a wall, turn around
        if (!isGrounded || isBlocked || isBlocked2)
        {
            Vector3 currRot = myTrans.eulerAngles;
            currRot.y += 180;
            myTrans.eulerAngles = currRot;
        }

   

        //Always move forward
        Vector2 myVel = myBody.velocity;
        myVel.x = -myTrans.right.x * speed;
        myBody.velocity = myVel;
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Enemy")
        {
            Vector3 currRot = myTrans.eulerAngles;
            currRot.y += 180;
            myTrans.eulerAngles = currRot;
        }

        if(other.gameObject.tag == "Player")
        {
            print("collision");
            Fmod.updateChannelPos(transform.position.x, transform.position.y, transform.position.z);
            Fmod.updateChannelVel(0f, 0f, 0f);
            Fmod.playSounds(2);
            ScoreMgr.healthPoint -= 1;
        }
    }
}