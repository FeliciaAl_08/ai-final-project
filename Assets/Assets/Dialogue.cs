﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;

using System.Collections.Generic;

using UnityEngine.UI;
using System.Runtime.InteropServices;

public class Dialogue : MonoBehaviour
{
    public Text labelText;
    public Text scoreText;
    bool notdone = true;

    float timer = 0.0f;
    private int numLine = -1;
    private string filename = "Read.txt";

    void Start()
    {
        labelText.text = " ";
        scoreText.text = " " +
            "\n Press '+' to read lines and go to next line!" +
            "\nPress '-' to go to previous line!";
    }

    void Update()
    {
        DLLTest.Init(filename);

        if (Input.GetKeyDown(KeyCode.KeypadPlus))
        {
            numLine++;
            labelText.text = Marshal.PtrToStringAnsi(DLLTest.getCharacterName(numLine)) + " ";
            scoreText.text = Marshal.PtrToStringAnsi(DLLTest.getLine(numLine));
        }
        else if (Input.GetKeyDown(KeyCode.KeypadMinus))
        {
            numLine--;
            labelText.text = Marshal.PtrToStringAnsi(DLLTest.getCharacterName(numLine)) + " ";
            scoreText.text = Marshal.PtrToStringAnsi(DLLTest.getLine(numLine));
        }

        timer += Time.deltaTime;

        

    }

}