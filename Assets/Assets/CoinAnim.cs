﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAnim : MonoBehaviour {

    Vector3 startPos;

    float newpos;

    static float t = 0.0f;
	// Use this for initialization
	void Start ()
    {
        startPos = gameObject.transform.position;

        newpos = startPos.y + 5.0f;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (enabled)
        {
            //newpos = startPos.y + 5.0f;
            gameObject.transform.position = new Vector3(startPos.x, Mathf.Lerp(startPos.y, newpos, t), startPos.z);

            t += 0.15f * Time.deltaTime;

            if (t > 1.0f)
            {
                float temp = startPos.y;
                startPos.y = newpos;
                newpos = temp;

                t = 0.0f;
            }
        }

        if (!enabled) return;
	}
}
