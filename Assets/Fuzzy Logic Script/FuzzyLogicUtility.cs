﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FuzzyLogicUtility : MonoBehaviour
{
    public struct EvalName
    {
        public NameID nameID;
        public float eval;
    }

    public struct NameID
    {
        public int id;
        public string name;
    }

    public struct FuzzPoint
    {
        public TopBottom isClampedToTop;
        public float xPosition;
    }
    public enum TopBottom
    {
        Bottom = 0, Top = 1
    };

    public static List<FuzzySet> fuzzySets = new List<FuzzySet>();

    public class FuzzySet
    {
        public FuzzPoint left, center, right;
        public NameID nameID;
        EvalName temp;
        //EvalName a, b;
        public FuzzySet()
        {
            nameID.id = 0;
            nameID.name = "Fuzzy Set";
            left.xPosition = 0;
            left.isClampedToTop = TopBottom.Bottom;
            center.xPosition = 1;
            center.isClampedToTop = TopBottom.Top;
            right.xPosition = 2;
            right.isClampedToTop = TopBottom.Bottom;
        }

        public FuzzySet(NameID a_name, FuzzPoint a_leftPoint, FuzzPoint a_rightPoint)
        {
            nameID = a_name;
            left = a_leftPoint;
            if (a_leftPoint.isClampedToTop == TopBottom.Bottom)
                center = a_rightPoint;
            else if (a_leftPoint.isClampedToTop == TopBottom.Top)
                center = a_leftPoint;
            right = a_rightPoint;
        }

        public FuzzySet(NameID a_name, FuzzPoint a_leftPoint, FuzzPoint a_centerPoint, FuzzPoint a_rightPoint)
        {
            nameID = a_name;
            left = a_leftPoint;
            center = a_centerPoint;
            right = a_rightPoint;
        }

        public List<EvalName> EvaluateGraph(float x)
        {
            List<EvalName> values = new List<EvalName>();

            for (var i = 0; i < fuzzySets.Count; i++)
            {
                temp.eval = fuzzySets[i].Evaluate(x);
                temp.nameID = fuzzySets[i].nameID;
                values.Add(temp);
            }

            values.Sort((a, b) => a.eval.CompareTo(b.eval));

            //values.Sort(SortEval);

            return values;
        }    

        public float Evaluate(float x)
        {
            float evaluatedA = mix((float)left.isClampedToTop, (float)center.isClampedToTop, Mathf.SmoothStep(left.xPosition, center.xPosition + 0.0001f, x));
            float evaluatedB = mix((float)center.isClampedToTop,(float)right.isClampedToTop,Mathf.SmoothStep(center.xPosition, right.xPosition + 0.0001f,x));
            return (evaluatedA * evaluatedB);
        }
    }    

    public static float mix(float x, float a, float y)
    {
        return x * (1.0f - a) + y * a;
    }
}
