﻿using System.IO;
using System;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Collections.Generic;

namespace XMLUl
{


    /// code referenced from:
    /// http://wiki.unity3d.com/index.php?title=Save_and_Load_from_XML
    /// 
    public class XML_Utility
    {
        public static string UTFbyteToString(byte[] ch)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            string constString = encoding.GetString(ch);

            return (constString);
        }

        public static byte[] UTFStringtoByte(string XMLString)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            byte[] byteArray = encoding.GetBytes(XMLString);

            return byteArray;
        }

        public static string serializeObject(object serializeObj)
        {
            string StringXML = null;
            MemoryStream memoryStream = new MemoryStream();
            XmlSerializer xs = new XmlSerializer(typeof(List<SerializeData.ListOfItem>));
            XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);

            xs.Serialize(xmlTextWriter, serializeObj);
            memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
            StringXML = UTFbyteToString(memoryStream.ToArray());
            return StringXML;
        }

        public static object deserializeObject(string a_StringXML) // deserialize object
        {
            XmlSerializer xs = new XmlSerializer(typeof(List<SerializeData.ListOfItem>));
            MemoryStream memoryStream = new MemoryStream(UTFStringtoByte(a_StringXML));
            return xs.Deserialize(memoryStream);
        }
    }

    [Serializable()]
    public class SerializeData : ISerializable
    {
        [Serializable()]
        public class ListOfItem : ISerializable
        {
            public string ID;
            public string Name;
            public float posx;
            public float posy;
            public float posz;

            public ListOfItem()
            {
                ID = string.Empty;
                Name = string.Empty;
                posx = 0;
                posy = 0;
                posz = 0;
            }

            // Deserialization
            public ListOfItem(SerializationInfo info, StreamingContext ctxt)
            {
                ID = (String)info.GetValue("ID", typeof(string));
                Name = (String)info.GetValue("Name", typeof(string));
                posx = (float)info.GetValue("posx", typeof(float));
                posy = (float)info.GetValue("posy", typeof(float));
                posz = (float)info.GetValue("posz", typeof(float));
            }

            // Serialization
            public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
            {
                info.AddValue("ID", ID);
                info.AddValue("Name", Name);
                info.AddValue("posx", posx);
                info.AddValue("posy", posy);
                info.AddValue("posz", posz);
            }
        }

        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
        }
    }
}

