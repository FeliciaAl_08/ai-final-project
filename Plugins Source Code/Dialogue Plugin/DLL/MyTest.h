#pragma once

#ifdef FUNCDLL_EXPORT
#define DLL_API __declspec(dllexport) 
#else
#define DLL_API __declspec(dllimport) 
#endif

#include <fstream>
#include <string>
#include <vector>

extern "C"
{
	DLL_API float TestMultiply(float a, float b);
	DLL_API float TestAdd(float a, float b);
	DLL_API void Init(const char *filename);
	DLL_API int getNum(); //Returns number of dialogue lines
	DLL_API const char *getLine(int ID); //Given an ID returns the associated dialogue line
	DLL_API const char *getCharacterName(int ID); //Given an ID returns the associated character name

	std::vector<std::string> lines;
}

class MyTest
{
public:
	MyTest();
	~MyTest();

};

